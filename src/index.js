const Telegraf = require('telegraf');

const { uploader, SUPPORTED_SUB_TYPES } = require('./middlewares/uploader');

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').load();
}

const bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN);

bot.on(SUPPORTED_SUB_TYPES, uploader);

bot.startPolling();
