const createWTClient = require('@wetransfer/js-sdk');

const uploadToWeTransfer = async (contentBuffer, fileName) => {
  const apiClient = await createWTClient(process.env.WETRANSFER_API_KEY, {
    // logger: {
    //   level: 'error',
    // },
  });
  const transfer = await apiClient.transfer.create({ name: 'Bot transfer' });

  const transferFiles = await apiClient.transfer.addFiles(transfer, [
    { filename: fileName, filesize: contentBuffer.byteLength },
  ]);

  await Promise.all(
    transferFiles
      .filter(item => item.content_identifier === 'file')
      .map(file => apiClient.transfer.uploadFile(file, contentBuffer))
  );

  return transfer.shortened_url;
};

module.exports = uploadToWeTransfer;
