const request = require('request');

const downloadFromTelegram = url =>
  new Promise((resolve, reject) => {
    request({ url, encoding: null }, (error, response, body) => {
      if (error) {
        return reject(error);
      }

      if (response.statusCode === 200) {
        resolve(body);
      }
    });
  });

module.exports = downloadFromTelegram;
