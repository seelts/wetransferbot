const downloadFromTelegram = require('../utils/downloadFromTelegram');
const uploadToWeTransfer = require('../utils/uploadToWeTransfer');

const getFileFromPhoto = photoSizes => ({
  id: photoSizes.sort((s1, s2) => s2.file_size > s1.file_size)[0].file_id,
  name: 'image.jpg',
});

const getFileFromDocument = document => ({
  id: document.file_id,
  name: document.file_name,
});

const fileGetters = { photo: getFileFromPhoto, document: getFileFromDocument };

const upload = fileName => contentBuffer =>
  uploadToWeTransfer(contentBuffer, fileName);

const uploader = ctx => {
  const files = ctx.updateSubTypes.reduce(
    (result, subType) => [
      ...result,
      fileGetters[subType](ctx.message[subType]),
    ],
    []
  );

  ctx.replyWithChatAction('upload_document');

  const promises = files.map(file =>
    ctx.telegram
      .getFileLink(file.id)
      .then(downloadFromTelegram)
      .then(upload(file.name))
      .then(url => {
        ctx.reply(`Yep, it is uploaded!\nHere is the url: ${url}`);
      })
      .catch(() => {
        ctx.reply('Oops, there was an error, sorry :(');
      })
  );

  return Promise.all(promises);
};

module.exports = { uploader, SUPPORTED_SUB_TYPES: Object.keys(fileGetters) };
